var app = angular.module("punterest", ["ngMaterial", "ngRoute", "firebase"]);

app.controller("mainController", ["$scope", "$firebase",
    function($scope, $firebase) {
        var ref = new Firebase("https://punterest.firebaseio.com/users");
        var sync = $firebase(ref);
        $scope.users = sync.$asArray();

        $scope.addUser = function($event) {
            if (!$event || $event.keyCode == 13) {
                $scope.users.$add({name: $scope.newUser, points: 0});
                $scope.newUser = "";
            }
        };
    }
]);

app.run();